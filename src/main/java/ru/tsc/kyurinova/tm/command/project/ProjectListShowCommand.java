package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable final List<Project> projects;
        System.out.println("[LIST PROJECTS]");
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortType.getComparator());
        }
        for (@NotNull final Project project : projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " + project.getId() + ". " + project.getName() + ". " + project.getDescription() + ". " + project.getStatus() + ". " + project.getStartDate());
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
