package ru.tsc.kyurinova.tm.exception.user;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
